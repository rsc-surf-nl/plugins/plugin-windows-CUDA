$ProgressPreference = 'SilentlyContinue'

# $ErrorActionPreference = "Stop"
 
function DownloadCuda([string] $url, [string] $downloadLocation, [int] $retries)
{
    while($true)
    {
        try
        {
            Invoke-WebRequest $url -OutFile $downloadLocation
            break
        }
        catch
        {
            $exceptionMessage = $_.Exception.Message
            Write-Host "Failed to download '$url': $exceptionMessage"
            if ($retries -gt 0) {
                $retries--
                Write-Host "Waiting 10 seconds before retrying. Retries left: $retries"
                Start-Sleep -Seconds 10
 
            }
            else
            {
                $exception = $_.Exception
                throw $exception
            }
        }
    }
}

try {
    DownloadCuda -url "https://developer.download.nvidia.com/compute/cuda/11.7.0/local_installers/cuda_11.7.0_516.01_windows.exe" -downloadLocation ".\cuda_driver.exe" -retries 6
    Start-Process "cuda_driver.exe" -argumentlist "-s" -wait
} catch {
    Write-Host "CUDA installation has failed with the following error: $_"
    Throw "Aborted cuda installation returned $_"
}
